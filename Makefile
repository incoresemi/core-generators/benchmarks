include Makefile.inc

linesize=$$(($(xlen)/8))
target ?= CUSTOM
ITERATIONS ?= 5000
RISCV_PREFIX ?= riscv$(xlen)-unknown-elf-
RISCV_GCC ?= $(RISCV_PREFIX)gcc
RISCV_LINK_OPTS ?= -static -nostartfiles -lgcc -T ./common/link.ld
RISCV_HEX = elf2hex $(linesize) $$((4194304*64/$(xlen)))
RISCV_OBJDUMP ?= $(RISCV_PREFIX)objdump -D
OUTDIR ?= output
FLAGS_STR = -mcmodel=medany -D$(target) -DPERFORMANCE_RUN=1 -DMAIN_HAS_NOARGC=1 -DHAS_STDIO \
					  -DHAS_PRINTF -DHAS_TIME_H -DUSE_CLOCK -DHAS_FLOAT=0 -DITERATIONS=$(ITERATIONS) \
						-O3 -fno-common -funroll-loops -finline-functions -fselective-scheduling \
						-falign-functions=16 -falign-jumps=4 -falign-loops=4 -finline-limit=1000 \
					 	-nostartfiles -nostdlib -ffast-math -fno-builtin-printf -march=$(march)\
						-mexplicit-relocs -ffreestanding -fno-builtin -mtune=rocket

FLAGS = "$(FLAGS_STR)"


.PHONY: dhrystone
dhrystone: 
	@echo "Compiling Dhrystone"
	@mkdir -p output/
	$(RISCV_GCC) -I./common -D$(target) -DCONFIG_RISCV64=True -mcmodel=medany -static -std=gnu99\
		-O2 -ffast-math -fno-common -fno-builtin-printf -march=$(march) -mabi=$(mabi) -w -static -mtune=rocket\
		-nostartfiles -lgcc -c common/crt.S -o output/crt.o -fno-tree-loop-distribute-patterns
	$(RISCV_GCC) -I./common -D$(target) -DCONFIG_RISCV64=True -mcmodel=medany -static -std=gnu99\
		-O2 -ffast-math -fno-common -fno-builtin-printf -march=$(march) -mabi=$(mabi) -w -static -mtune=rocket\
		-nostartfiles -lgcc -c common/syscalls.c -o output/syscalls.o -fno-tree-loop-distribute-patterns
	$(RISCV_GCC) -I./common -I./dhrystone -DCONFIG_RISCV64=True \
				-DITERATIONS=$(ITERATIONS) -D$(target)=True -mcmodel=medany -static -std=gnu99 -O2 -ffast-math \
				-fno-common -fno-builtin-printf -march=$(march) -mabi=$(mabi) -mtune=rocket\
				-w -static -nostartfiles -lgcc -T ./common/link.ld -o $(OUTDIR)/dhry.riscv \
				./dhrystone/dhry_1.c ./dhrystone/dhry_2.c ./output/syscalls.o ./output/crt.o -fno-tree-loop-distribute-patterns
	$(RISCV_OBJDUMP) $(OUTDIR)/dhry.riscv > $(OUTDIR)/dhry.dump
	$(RISCV_HEX) $(OUTDIR)/dhry.riscv 2147483648 > $(OUTDIR)/code.mem


.PHONY: hello
hello:
	@echo "Compiling Hello"
	@mkdir -p output/
	$(RISCV_GCC) -I./common -I./hello -DCONFIG_RISCV64=True \
				-D$(target)=True -mcmodel=medany -static -std=gnu99 -O2 -ffast-math \
				-fno-common -fno-builtin-printf -march=$(march) -mabi=$(mabi) -w -static \
				-nostartfiles -lgcc -T ./common/link.ld -o $(OUTDIR)/hello.riscv ./hello/hello.c \
				./common/syscalls.c ./common/crt.S
	@$(RISCV_OBJDUMP) $(OUTDIR)/hello.riscv > $(OUTDIR)/hello.dump
	@$(RISCV_HEX) $(OUTDIR)/hello.riscv 2147483648 > $(OUTDIR)/code.mem

.PHONY: coremarks
coremarks:
	@echo "Compiling Coremarks"
	@mkdir -p output/
	@$(RISCV_GCC) -I./common -D$(target) -DCONFIG_RISCV64=True -mcmodel=medany -static -std=gnu99\
		-O2 -ffast-math -fno-common -fno-builtin-printf -march=$(march) -mabi=$(mabi) -w -static -mtune=rocket\
		-nostartfiles -lgcc -c common/crt.S -o output/crt.o
	@$(RISCV_GCC) -I./common -D$(target) -DCONFIG_RISCV64=True -mcmodel=medany -static -std=gnu99\
		-O2 -ffast-math -fno-common -fno-builtin-printf -march=$(march) -mabi=$(mabi) -w -static -mtune=rocket\
		-nostartfiles -lgcc -c common/syscalls.c -o output/syscalls.o
	$(RISCV_GCC) -I./common -I./coremarks $(FLAGS_STR) $(RISCV_LINK_OPTS) -o $(OUTDIR)/coremarks.riscv \
		./coremarks/core_util.c ./coremarks/ee_printf.c ./coremarks/core_state.c \
		-DFLAGSTR=\"$(FLAGS)\" \
		./coremarks/core_list_join.c ./coremarks/core_portme.c ./coremarks/core_main.c \
		./coremarks/core_matrix.c ./output/crt.o ./output/syscalls.o
	@$(RISCV_OBJDUMP) $(OUTDIR)/coremarks.riscv > $(OUTDIR)/coremarks.dump
	@$(RISCV_HEX) $(OUTDIR)/coremarks.riscv 2147483648 > $(OUTDIR)/code.mem


.PHONY: clean
clean:
	rm -rf $(OUTDIR)
